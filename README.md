# @ki2/feathersjs-innerhooks

## Access functions

- `useApp`
- `useCanSkip`
- `useData`
- `useError`
- `useHadResult`
- `useId.ts`
  - `useId`
  - `useStringId`
- `useIsInternal`
- `useLocals`
- `useMethod`
- `useParams`
- `usePath`
- `useProvider`
- `useQuery.ts`
  - `useQuery`
  - `useSetQuery`
- `useResult`
- `useRoute`

## Helpers functions

- `convertId`
- `extractResult.ts`
  - `extractResult`
  - `extractAll`
  - `extractOne`
- `genHookContext`
- `genPaginated`
- `genParams`
- `password.ts`
  - `comparePassword`
  - `hastPassword`

## Validation functions

- `isContext`

## Types

```ts
// InnerHookContext.ts

type InnerHookContext = HookContext | Params;
```
