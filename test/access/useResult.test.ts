import { useResult, genHookContext } from "../../src";

describe("test useResult innerhook", () => {
  it("should return default result", () => {
    const context = genHookContext();
    expect(useResult(context)).toBe(undefined);
  });

  it("should return custom result", () => {
    const context = genHookContext({
      result: {
        data: true,
      },
    });
    expect(useResult(context)).toEqual({ data: true });
  });

  it("should return custom dispatch", () => {
    const context = genHookContext({
      dispatch: {
        data: true,
      },
    });
    expect(useResult(context)).toEqual({ data: true });
  });

  it("should return custom dispatch (with result)", () => {
    const context = genHookContext({
      result: {
        test: false,
      },
      dispatch: {
        data: true,
      },
    });
    expect(useResult(context)).toEqual({ data: true });
  });
});
