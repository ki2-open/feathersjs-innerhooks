import { useRoute, genHookContext, genParams } from "../../src";

describe("test useRoute innerhook", () => {
  it("should return default route from HookContext", () => {
    const context = genHookContext();
    expect(useRoute(context)).toEqual({});
  });

  it("should return default route from Params", () => {
    const params = genParams();
    expect(useRoute(params)).toEqual({});
  });

  it("should return custom route from HookContext", () => {
    const params = genParams({
      route: {
        test: "test",
      },
    });
    const context = genHookContext({ params });
    expect(useRoute(context)).toEqual({ test: "test" });
  });

  it("should return custom route from Params", () => {
    const params = genParams({
      route: {
        test: "test",
      },
    });
    expect(useRoute(params)).toEqual({ test: "test" });
  });
});
