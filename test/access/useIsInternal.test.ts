import { useIsInternal, genHookContext, genParams } from "../../src";

describe("test useIsInternal innerhook", () => {
  it("should return false", () => {
    const params = genParams({ provider: "something" });
    expect(useIsInternal(params)).toBe(false);
    const context = genHookContext({ params });
    expect(useIsInternal(context)).toBe(false);
  });

  it("shloud return true", () => {
    const params = genParams({ provider: undefined });
    expect(useIsInternal(params)).toBe(true);
    const context = genHookContext({ params });
    expect(useIsInternal(context)).toBe(true);
  });
});
