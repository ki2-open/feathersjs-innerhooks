import { useApp, genHookContext } from "../../src";

describe("test useApp innerhook", () => {
  it("should return default app", () => {
    const context = genHookContext();
    expect(useApp(context)).toEqual({});
  });

  it("should return custom app", () => {
    const context = genHookContext({
      app: "test-app" as any,
    });
    expect(useApp(context)).toBe("test-app");
  });
});
