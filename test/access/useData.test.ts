import { useData, genHookContext } from "../../src";

describe("test useData innerhook", () => {
  it("should return default data", () => {
    const context = genHookContext();
    expect(useData(context)).toBe(undefined);
  });

  it("should return custom data", () => {
    const context = genHookContext({
      data: {
        a: 1,
        b: true,
      },
    });
    expect(useData(context)).toEqual({
      a: 1,
      b: true,
    });
  });
});
