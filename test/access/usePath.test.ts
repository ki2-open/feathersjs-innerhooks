import { usePath, genHookContext } from "../../src";

describe("test usePath innerhook", () => {
  it("should return default path", () => {
    const context = genHookContext();
    expect(usePath(context)).toBe("/");
  });

  it("should return custom path", () => {
    const context = genHookContext({
      path: "/test",
    });
    expect(usePath(context)).toBe("/test");
  });
});
