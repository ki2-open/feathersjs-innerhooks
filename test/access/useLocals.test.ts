import { useLocals, genHookContext, genParams } from "../../src";

describe("test useLocals innerhook", () => {
  it("should return undefined on default locals (from Params)", () => {
    const params = genParams();
    const { getLocal } = useLocals(params);
    expect(getLocal("key")).toBe(undefined);
  });

  it("should return undefined on default locals (from HookContext)", () => {
    const params = genParams();
    const context = genHookContext({ params });
    const { getLocal } = useLocals(context);
    expect(getLocal("key")).toBe(undefined);
  });

  it("should set and get locals (from Params)", () => {
    const params = genParams();
    const { getLocal, setLocal } = useLocals(params);
    setLocal("key", true);
    expect(getLocal("key")).toBe(true);
  });

  it("should set and get locals (from HookContext)", () => {
    const params = genParams();
    const context = genHookContext({ params });
    const { getLocal, setLocal } = useLocals(context);
    setLocal("key", true);
    expect(getLocal("key")).toBe(true);
  });
});
