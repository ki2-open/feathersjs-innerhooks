import { useParams, genHookContext, genParams } from "../../src";

describe("test useParams innerhook", () => {
  it("should return default params from context", () => {
    const params = genParams();
    const context = genHookContext({ params });
    expect(useParams(context)).toEqual(params);
  });

  it("should return custom params from context", () => {
    const params = genParams({
      headers: { test: true },
    });
    const context = genHookContext({ params });
    expect(useParams(context)).toEqual(params);
  });

  it("should return default params (no context)", () => {
    const params = genParams();
    expect(useParams(params)).toEqual(params);
  });

  it("should return custom params (no context)", () => {
    const params = genParams({
      query: { id: 0 },
    });
    expect(useParams(params)).toEqual(params);
  });
});
