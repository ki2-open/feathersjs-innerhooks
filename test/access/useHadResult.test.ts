import { useHadResult, genHookContext } from "../../src";

describe("test useHadResult innerhook", () => {
  it("should return true", () => {
    let context = genHookContext({
      result: { data: true },
    });
    expect(useHadResult(context)).toBe(true);
    context = genHookContext({
      dispatch: { data: true },
    });
    expect(useHadResult(context)).toBe(true);
    context = genHookContext({
      result: {},
    });
    expect(useHadResult(context)).toBe(true);
  });

  it("should return false", () => {
    let context = genHookContext();
    expect(useHadResult(context)).toBe(false);
  });
});
