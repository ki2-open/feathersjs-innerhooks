import { useId, genHookContext, useStringId } from "../../src";

describe("test useId innerhook", () => {
  it("should return null or undefined", () => {
    let context = genHookContext();
    expect(useId(context)).toBe(undefined);
    context = genHookContext({ id: null });
    expect(useId(context)).toBe(null);
  });

  it("should return id", () => {
    let context = genHookContext({ id: "test" });
    expect(useId(context)).toBe("test");
    context = genHookContext({ id: 123 });
    expect(useId(context)).toBe(123);
  });
});

describe("test useStringId innerhook", () => {
  it("should return null", () => {
    const context = genHookContext();
    expect(useStringId(context)).toBe(null);
  });

  it("should return string id", () => {
    let context = genHookContext({ id: "test" });
    expect(useStringId(context)).toBe("test");
    context = genHookContext({ id: 123 });
    expect(useStringId(context)).toBe("123");
  });
});
