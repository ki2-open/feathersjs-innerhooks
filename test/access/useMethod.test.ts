import { useMethod, genHookContext } from "../../src";

describe("test useMethod innerhook", () => {
  it("should return default method", () => {
    const context = genHookContext();
    expect(useMethod(context)).toBe("create");
  });

  it("should return custom method", () => {
    const context = genHookContext({
      method: "remove",
    });
    expect(useMethod(context)).toBe("remove");
  });
});
