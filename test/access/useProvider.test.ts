import { useProvider, genHookContext, genParams } from "../../src";

describe("test useProvider innerhook", () => {
  it("should return default provider from context", () => {
    const context = genHookContext();
    expect(useProvider(context)).toBe("test");
  });

  it("should return default provider from params", () => {
    const params = genParams();
    expect(useProvider(params)).toBe("test");
  });

  it("should return custom provider from context", () => {
    const params = genParams({ provider: "test2" });
    const context = genHookContext({ params });
    expect(useProvider(context)).toBe("test2");
  });

  it("should return custom provider from params", () => {
    const params = genParams({ provider: "test2" });
    expect(useProvider(params)).toBe("test2");
  });
});
