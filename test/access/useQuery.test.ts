import {
  useParams,
  useQuery,
  useSetQuery,
  genHookContext,
  genParams,
} from "../../src";

describe("test useQuery innerhook", () => {
  it("should return default query", () => {
    const params = genParams();
    expect(useQuery(params)).toEqual({});
    const context = genHookContext({ params });
    expect(useQuery(context)).toEqual({});
    const context2 = genHookContext();
    expect(useQuery(context2)).toEqual({});
  });

  it("should return custom query", () => {
    const params = genParams({ query: { id: 123 } });
    expect(useQuery(params)).toEqual({ id: 123 });
    const context = genHookContext({ params });
    expect(useQuery(context)).toEqual({ id: 123 });
  });

  it("should allow to edit default query (from Params)", () => {
    const params = genParams();
    const setQuery = useSetQuery(params);
    setQuery({ id: 123 });
    expect(useQuery(params)).toEqual({ id: 123 });
    const context = genHookContext({ params });
    expect(useQuery(context)).toEqual({ id: 123 });
  });

  it("should allow to edit default query (from HookContext)", () => {
    const params = genParams();
    const context = genHookContext({ params });
    const setQuery = useSetQuery(context);
    setQuery({ id: 123 });
    const params2 = useParams(context);
    expect(useQuery(params2)).toEqual({ id: 123 });
    expect(useQuery(context)).toEqual({ id: 123 });
  });

  it("should allow to edit custum query (from Params)", () => {
    const params = genParams({ query: { id: 123 } });
    const setQuery = useSetQuery(params);
    setQuery({ data: true });
    expect(useQuery(params)).toEqual({ data: true });
    const context = genHookContext({ params });
    expect(useQuery(context)).toEqual({ data: true });
  });

  it("should allow to edit custum query (from HookContext)", () => {
    const params = genParams({ query: { id: 123 } });
    const context = genHookContext({ params });
    const setQuery = useSetQuery(context);
    setQuery({ data: true });
    const params2 = useParams(context);
    expect(useQuery(params2)).toEqual({ data: true });
    expect(useQuery(context)).toEqual({ data: true });
  });
});
