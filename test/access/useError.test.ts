import { useError, genHookContext } from "../../src";

describe("test useError innerhook", () => {
  it("should return default error", () => {
    const context = genHookContext();
    expect(useError(context)).toBe(undefined);
  });

  it("should return custom error", () => {
    const context = genHookContext({
      error: {
        code: 404,
      },
    });
    expect(useError(context)).toEqual({
      code: 404,
    });
  });
});
