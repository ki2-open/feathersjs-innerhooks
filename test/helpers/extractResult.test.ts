import { extractResult, extractAll, extractOne, genPaginated } from "../../src";

describe("test extractResult function", () => {
  it("should return array", () => {
    expect(extractResult([5, 6])).toEqual([5, 6]);
    expect(extractResult(genPaginated([5, 6]))).toEqual([5, 6]);
  });

  it("should return first value", () => {
    expect(extractResult([5, 6], true)).toBe(5);
    expect(extractResult(genPaginated([5, 6]), true)).toBe(5);
  });

  it("should return undefined", () => {
    expect(extractResult(5 as any)).toBe(undefined);
    expect(extractResult([])).toBe(undefined);
  });
});

describe("test extractAll function", () => {
  it("should return item as array", () => {
    expect(extractAll("test")).toEqual(["test"]);
    expect(extractAll(5)).toEqual([5]);
  });

  it("should return array directly", () => {
    expect(extractAll([5])).toEqual([5]);
    expect(extractAll([5, 6])).toEqual([5, 6]);
    expect(extractAll(["test"])).toEqual(["test"]);
  });

  it("should return paginated item as array", () => {
    expect(extractAll(genPaginated("test"))).toEqual(["test"]);
    expect(extractAll(genPaginated(5))).toEqual([5]);
  });

  it("should return paginated array as array", () => {
    expect(extractAll(genPaginated([5, 6]))).toEqual([5, 6]);
    expect(extractAll(genPaginated(["test"]))).toEqual(["test"]);
  });

  it("should return empty array", () => {
    expect(extractAll(null)).toEqual([]);
    expect(extractAll(undefined)).toEqual([]);
    expect(extractAll(genPaginated([]))).toEqual([]);
  });
});

describe("test extractOne function", () => {
  it("should return item", () => {
    expect(extractOne("test")).toBe("test");
    expect(extractOne(5)).toBe(5);
  });

  it("should return first item of an array", () => {
    expect(extractOne([5, 6])).toBe(5);
    expect(extractOne(["test"])).toBe("test");
  });

  it("should return item (paginated)", () => {
    expect(extractOne(genPaginated("test"))).toBe("test");
    expect(extractOne(genPaginated(5))).toBe(5);
  });

  it("should return first item of an array (paginated)", () => {
    expect(extractOne(genPaginated([5, 6]))).toBe(5);
    expect(extractOne(genPaginated(["test"]))).toBe("test");
  });

  it("should return null", () => {
    expect(extractOne(null)).toBe(null);
    expect(extractOne(undefined)).toBe(null);
    expect(extractOne([])).toBe(null);
  });
});
