import { genHookContext, genParams, DEFAULT_HOOKCONTEXT } from "../../src";

describe("test genHookContext", () => {
  it("should return default object", () => {
    expect(genHookContext()).toEqual(DEFAULT_HOOKCONTEXT);
  });

  it("should return edited object", () => {
    expect(
      genHookContext({
        method: "patch",
        type: "after",
      })
    ).toEqual({
      ...DEFAULT_HOOKCONTEXT,
      method: "patch",
      type: "after",
    });

    const out = genHookContext({
      params: { headers: { test: true } },
    });

    expect(out.params.headers["test"]).toBe(true);
    expect(out.params.provider).toBe("test");
  });

  it("should not return default object (if edited)", () => {
    expect(genHookContext({ method: "find" })).not.toEqual(DEFAULT_HOOKCONTEXT);
    expect(genHookContext({ params: { query: { test: true } } })).not.toEqual(
      DEFAULT_HOOKCONTEXT
    );
  });
});
