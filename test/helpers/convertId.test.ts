import { convertId } from "../../src";

import { newObjectId } from "@ki2/utils-extra";

describe("test converId helper function", () => {
  it("should return null", () => {
    expect(convertId(null)).toBe(null);
    expect(convertId(undefined)).toBe(null);
  });

  it("should return string id", () => {
    expect(convertId("test")).toBe("test");
    expect(convertId(123)).toBe("123");
    const oid = newObjectId();
    expect(convertId(oid)).toBe(String(oid));
  });
});
