import { genPaginated } from "../../src";

describe("test genPaginated", () => {
  it("should return valid paginated object", () => {
    expect(genPaginated(2)).toEqual({
      data: [2],
      total: 1,
      limit: 0,
      skip: 0,
    });
    expect(genPaginated(null)).toEqual({
      data: [null],
      total: 1,
      limit: 0,
      skip: 0,
    });
    expect(genPaginated([9, 3])).toEqual({
      data: [9, 3],
      total: 2,
      limit: 0,
      skip: 0,
    });
  });
});
