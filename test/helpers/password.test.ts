import { comparePassword, hashPassword } from "../../src";

describe("test hast & compare password helper functions", () => {
  it("should hash & compare with default hash size", async () => {
    const password = "azerty";
    const hashed = await hashPassword(password);
    expect(hashed).not.toBe(password);
    expect(await comparePassword(password, hashed)).toBe(true);
  });

  it("should hash & compare with custom number hash size", async () => {
    const password = "azerty";
    const hashed = await hashPassword(password, 2);
    expect(hashed).not.toBe(password);
    expect(await comparePassword(password, hashed)).toBe(true);
  });

  it.skip("should hash & compose with app defined hash size", () => {
    // TODO
  });
});
