import { genParams, DEFAULT_PARAMS } from "../../src";

describe("test genParams", () => {
  it("should return default object", () => {
    expect(genParams()).toEqual(DEFAULT_PARAMS);
  });

  it("should return edited object", () => {
    expect(
      genParams({
        query: {
          id: 0,
        },
      })
    ).toEqual({
      ...DEFAULT_PARAMS,
      query: {
        id: 0,
      },
    });

    expect(
      genParams({
        headers: {
          "X-TEST": "test",
        },
      })
    ).toEqual({
      ...DEFAULT_PARAMS,
      headers: {
        "X-TEST": "test",
      },
    });
  });

  it("should not return default object if edited", () => {
    expect(
      genParams({
        query: {
          id: 0,
        },
      })
    ).not.toEqual(DEFAULT_PARAMS);

    expect(
      genParams({
        headers: {
          "X-TEST": "test",
        },
      })
    ).not.toEqual(DEFAULT_PARAMS);
  });
});
