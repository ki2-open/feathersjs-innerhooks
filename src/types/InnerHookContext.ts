import { HookContext, Params } from "@feathersjs/feathers";

export type InnerHookContext = HookContext | Params;
