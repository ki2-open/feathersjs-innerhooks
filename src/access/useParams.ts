import type { Params } from "@feathersjs/feathers";

import type { InnerHookContext } from "..";
import { isContext } from "..";

declare module "@feathersjs/feathers" {
  interface Params {
    locals?: Map<string, any>;
  }
}

export function useParams(context: InnerHookContext): Params {
  if (isContext(context)) {
    return context.params;
  }
  return context;
}
