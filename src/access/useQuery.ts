import type { InnerHookContext } from "..";
import { useParams } from ".";

export function useQuery(context: InnerHookContext) {
  const params = useParams(context);
  return params.query as any;
}

export function useSetQuery(context: InnerHookContext) {
  const params = useParams(context);

  function setQuery(query: any) {
    params.query = query;
  }

  return setQuery;
}
