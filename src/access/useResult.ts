import type { HookContext } from "@feathersjs/feathers";

export function useResult(context: HookContext) {
  return context.dispatch ?? context.result;
}
