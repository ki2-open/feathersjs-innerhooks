import type { HookContext } from "@feathersjs/feathers";

import { convertId } from "..";

export function useId(context: HookContext) {
  return context.id;
}

export function useStringId(context: HookContext) {
  return convertId(useId(context));
}
