import type { HookContext } from "@feathersjs/feathers";

export function useData(context: HookContext) {
  return context.data;
}
