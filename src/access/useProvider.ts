import type { InnerHookContext } from "..";
import { useParams } from ".";

export type ProviderType = "rest" | "socketio" | "primus" | undefined;

export function useProvider(context: InnerHookContext) {
  const params = useParams(context);
  return params.provider as ProviderType;
}
