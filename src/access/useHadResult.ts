import type { HookContext } from "@feathersjs/feathers";

import { exist } from "@ki2/utils";

import { useResult } from ".";

export function useHadResult(context: HookContext) {
  const result = useResult(context);
  return exist(result);
}
