import type { HookContext } from "@feathersjs/feathers";

import { useHadResult } from ".";

export function useCanSkip(context: HookContext) {
  // Add more complicated condition in the futur if needed
  return useHadResult(context);
}
