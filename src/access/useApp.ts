import type { HookContext, Application } from "@feathersjs/feathers";
import { ServiceTypes } from "../declarations";

export function useApp(context: HookContext) {
  return context.app as Application<ServiceTypes>;
}
