import type { HookContext } from "@feathersjs/feathers";

export function useMethod(context: HookContext) {
  return context.method;
}
