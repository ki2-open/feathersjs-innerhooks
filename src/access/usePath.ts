import type { HookContext } from "@feathersjs/feathers";

export function usePath(context: HookContext) {
  return context.path;
}
