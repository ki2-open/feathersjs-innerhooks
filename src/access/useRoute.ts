import type { InnerHookContext } from "..";
import { useParams } from ".";

export function useRoute(context: InnerHookContext) {
  const params = useParams(context);
  return params.route;
}
