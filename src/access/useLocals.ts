import { exist } from "@ki2/utils";

import type { InnerHookContext } from "..";
import { useParams } from ".";

export type GetLocalFunction<T = any> = (key: string) => T | undefined;
export type SetLocalFunction<T = any> = (key: string, value: T) => void;

interface UseLocalsReturnType {
  getLocal: GetLocalFunction;
  setLocal: SetLocalFunction;
}

export function useLocals<T = any>(
  context: InnerHookContext
): UseLocalsReturnType {
  const params = useParams(context);

  function getLocal(key: string): T | undefined {
    const locals = params.locals;
    if (!exist(locals)) {
      return undefined;
    }
    return locals.get(key);
  }

  function setLocal(key: string, value: T) {
    if (!exist(params.locals)) {
      params.locals = new Map<string, any>();
    }
    const locals = params.locals;
    locals.set(key, value);
  }

  return {
    getLocal,
    setLocal,
  };
}
