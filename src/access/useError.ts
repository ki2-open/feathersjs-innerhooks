import type { HookContext } from "@feathersjs/feathers";

export function useError(context: HookContext) {
  return context.error;
}
