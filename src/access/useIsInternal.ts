import type { InnerHookContext } from "..";

import { useProvider } from ".";

export function useIsInternal(context: InnerHookContext) {
  const provider = useProvider(context);
  return provider === undefined;
}
