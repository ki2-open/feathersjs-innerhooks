import type { HookContext } from "@feathersjs/feathers";

import { exist } from "@ki2/utils";

export function isContext(value: any): value is HookContext {
  const hcv = value as Partial<HookContext>;
  if (!exist(hcv.app)) {
    return false;
  }

  if (!exist(hcv.method)) {
    return false;
  }

  if (!exist(hcv.params)) {
    return false;
  }

  if (!exist(hcv.path)) {
    return false;
  }

  if (!exist(hcv.service)) {
    return false;
  }

  if (!exist(hcv.type)) {
    return false;
  }

  return true;
}
