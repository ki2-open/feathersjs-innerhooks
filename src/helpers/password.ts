import * as bcrypt from "bcryptjs";
import { Application } from "../declarations";
import * as error from "@feathersjs/errors";

import { exist } from "@ki2/utils";

export async function comparePassword(
  clearpwd: string,
  hashpwd: string
): Promise<boolean> {
  return await bcrypt.compare(clearpwd, hashpwd);
}

const DEFAULT_HASH_SIZE = 10;

export async function hashPassword(
  pwd: string,
  numOrApp?: number | Application
): Promise<string> {
  let hashSize = DEFAULT_HASH_SIZE;
  if (exist(numOrApp)) {
    if (typeof numOrApp === "number") {
      hashSize = numOrApp;
    } else {
      hashSize =
        numOrApp.get("authentication.local.hashSize") ?? DEFAULT_HASH_SIZE;
    }
  }

  if (typeof hashSize !== "number") {
    throw new error.GeneralError("Invalid hash number");
  }

  return await bcrypt.hash(pwd, hashSize);
}
