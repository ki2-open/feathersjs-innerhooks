import type { HookContext, Application, Service } from "@feathersjs/feathers";

import { genParams } from "./genParams";

const DEFAULT_APP = {} as Application;
const DEFAULT_SERVICE = {} as Service<any>;
export const DEFAULT_HOOKCONTEXT = {
  app: DEFAULT_APP,
  method: "create",
  params: genParams(),
  path: "/",
  service: DEFAULT_SERVICE,
  type: "before",
} as HookContext;

export function genHookContext(edit: Partial<HookContext> = {}): HookContext {
  return {
    ...DEFAULT_HOOKCONTEXT,
    ...edit,
    params: genParams(edit.params),
  };
}
