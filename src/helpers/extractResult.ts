import { Paginated } from "@feathersjs/feathers";

import { exist } from "@ki2/utils";

export function extractResult<T>(
  result: Paginated<T> | Array<T>,
  onlyfirst: boolean = false
): T | Array<T> | undefined {
  let retarr: Array<T> | undefined = undefined;

  const presult: Paginated<T> = result as Paginated<T>;

  if (presult && presult.total !== undefined && presult.total > 0) {
    retarr = presult.data;
  } else if (Array.isArray(result) && result.length > 0) {
    retarr = result;
  }

  if (onlyfirst && retarr !== undefined) {
    return retarr[0];
  }

  return retarr;
}

export function extractAll<T>(
  result: Paginated<T> | Array<T> | T | undefined | null
): Array<T> {
  if (exist(result)) {
    const aresult = result as Array<T>;
    const presult = result as Paginated<T>;

    if (exist(presult.data) && presult.total >= 0) {
      return presult.data;
    } else if (Array.isArray(aresult)) {
      return aresult;
    } else {
      return [result as T];
    }
  }

  return [];
}

export function extractOne<T>(
  result: Paginated<T> | Array<T> | T | undefined | null
): T | null {
  if (!exist(result)) {
    return null;
  }

  const all = extractAll(result);
  if (all.length > 0) {
    return all[0];
  }

  return null;
}
