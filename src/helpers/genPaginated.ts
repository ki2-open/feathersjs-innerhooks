import { Paginated } from "@feathersjs/feathers";

interface IGenPaginatedOptions {
  limit?: number;
  skip?: number;
}

export function genPaginated<T>(
  data: T | T[],
  options: IGenPaginatedOptions = {}
): Paginated<T> {
  if (!Array.isArray(data)) {
    data = [data];
  }

  return {
    data,
    total: data.length,
    limit: options.limit ?? 0,
    skip: options.skip ?? 0,
  };
}
