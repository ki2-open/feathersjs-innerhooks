import type { Params } from "@feathersjs/feathers";

export const DEFAULT_PARAMS = {
  query: {},
  paginate: false,
  provider: "test",
  route: {},
  headers: {},
  users: undefined,
} as Params;

export function genParams(edit: Partial<Params> = {}): Params {
  return {
    ...DEFAULT_PARAMS,
    ...edit,
  };
}
