import { exist, isString } from "@ki2/utils";

export function convertId(id?: any | null): string | null {
  if (!exist(id)) {
    return null;
  }

  if (isString(id)) {
    return id;
  }

  return String(id);
}
